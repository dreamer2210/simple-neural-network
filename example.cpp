#include <iostream>
#include "neural.h"

using namespace std;

int main(int argc, char* argv[])
{
    // ways of creating
    // 1. with std::initializer_list<double>:
    neural n(3, {2, 3, 2});
    // 2. with size_n*:
    size_n count[3] = {2, 3, 2};
    neural n2(3, count);

    matrix input(2, 1);
    input[0][0] = 1;
    input[1][0] = 0;
    matrix output(2, 1);
    output[0][0] = 0;
    output[1][0] = 1;

    n2.GenerateWeights();
    cout << n2.Query(input, output) << endl;

    system("pause");

    return 0;
}