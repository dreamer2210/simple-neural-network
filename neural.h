#pragma once
#include <initializer_list>
#include <iostream>
#include <ctime>
#include <math.h>
#include "matrix.h"

typedef unsigned int size_n;

double sigmoid(double x);

class neural
{
    private:
        matrix* weights;
        size_n countMaxNeurons;
        size_n countLayers;
        size_n* countNeurons;
    public:
        neural(size_n layers, std::initializer_list<size_n> neurons);
        neural(size_n layers, size_n* neurons);
        ~neural();

        size_n GetCountLayer() { return this->countLayers; }
        size_n GetCountNeurons(size_n layer) { return this->countNeurons[layer]; }

        void GenerateWeights();
        matrix Query(const matrix& input, const matrix& output);
};