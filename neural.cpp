#include "neural.h"

double sigmoid(double x) 
{ 
    return 1/(1 + exp(-x)); 
}

neural::neural(size_n layers, std::initializer_list<size_n> neurons)
{
    srand(time(0));

    this->countLayers = layers;
    this->countNeurons = new size_n[layers];
    this->countMaxNeurons = 1;
    size_n i = 0;

    for(auto obj : neurons)
    {
        if(obj > this->countMaxNeurons)
            this->countMaxNeurons = obj;
        this->countNeurons[i] = obj;

        ++i;
    }

    this->weights = new matrix[layers - 1];

    for(i = 0; i < layers - 1; ++i)
    {
        weights[i].init(countNeurons[i + 1], countNeurons[i]);
    }
}

neural::neural(size_n layers, size_n* neurons)
{
    this->countLayers = layers;
    this->countNeurons = new size_n[layers];
    this->countMaxNeurons = 1;

    for(size_n i = 0; i < layers; ++i)
    {
        if(neurons[i] > this->countMaxNeurons)
            this->countMaxNeurons = neurons[i];
        countNeurons[i] = neurons[i];
    }

    this->weights = new matrix[layers - 1];

    for(size_n i = 0; i < layers - 1; ++i)
    {
        weights[i].init(countNeurons[i + 1], countNeurons[i]);
    }
}

neural::~neural()
{
    delete[] weights;
    delete[] countNeurons;
}

void neural::GenerateWeights()
{
    for(size_n i = 0; i < this->countLayers - 1; ++i)
        for(size_n j = 0; j < countNeurons[i + 1]; ++j)
            for(size_n k = 0; k < countNeurons[i]; ++k)
                weights[i][j][k] = double(rand() % 10000) / double(10000) - 0.5;
}

matrix neural::Query(const matrix& input, const matrix& output)
{
    matrix result(this->countMaxNeurons);
    matrix value = input;

    for(size_n i = 0; i < this->countLayers - 1; ++i)
        std::cout << weights[i] << std::endl;

    for(size_n i = 0; i < this->countLayers - 1; ++i)
    {
        value = weights[i] * value;

        std::cout << value << std::endl;

        for(size_n j = 0; j < value.getCountRows(); ++j)
            result[j][0] = sigmoid(value[j][0]);
        
        std::cout << result << std::endl;
    }

    return result;
}